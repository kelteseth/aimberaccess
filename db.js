const db =  require('better-sqlite3')("aimberaccess.sqlite3",{ verbose: console.log });

//Setup db
function initDB() {
    console.log("initDB");
    createTable();
}

//create databse table if needed
function createTable() {
    console.log("createTable()");
    //prepare statment
    var statement = db.prepare(`CREATE TABLE IF NOT EXISTS users 
	(
     id integer primary key autoincrement, 
     gitlabID integer,
     email varchar(100), 
     username varchar(50),
     displayname varchar(50),
     agreement_accepted INTEGER
    );`);
    //actually run the sql
    statement.run();
}

//INSERT
function insertUser(gitlabID, email, username, displayname) {
    var statement = db.prepare(`INSERT INTO users VALUES (null,?,?,?,?,0)`);
    statement.run(gitlabID, email, username.toLowerCase(), displayname);
}

//SELECT
function selectUserByEmail(email) {
    var statement = db.prepare(`SELECT * FROM users where email = ?`);
    return statement.get(email);
}

function selectUserByUsername(username) {
    var statement = db.prepare(`SELECT * FROM users where username = ?`);
    return statement.get(username.toLowerCase());
}

function selectUserByGitlabID(gitlabID){
    var statement = db.prepare(`SELECT * FROM users where gitlabID = ?`);
    return statement.get(gitlabID);
}

//UPDATE
function updateUserByUsernameWithAgreement(username, agreementAccepted){
    var statement = db.prepare("UPDATE users SET agreement_accepted = ? WHERE username = '?'");
    statement.run(agreementAccepted,username);
}


module.exports = {
    initDB,
    createTable,
    insertUser,
    selectUserByEmail,
    selectUserByUsername,
    selectUserByGitlabID,
    updateUserByUsernameWithAgreement
}